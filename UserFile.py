import datetime
import subprocess
import os

#Write User's name to a file
def writeUserToFile(name):
    delimiter = "\n"
    userfile = open("users.txt","a")
    userfile.write(name)
    userfile.write(delimiter)

#Fetch test users from file that is passed as argument
def getUserFromFile(filename):
    userfile = open(filename,"r")
    users = userfile.readlines()
    userfile.close()
    return users

#Crete New File based on date
def createFile():
    todayDate = datetime.datetime.now().strftime("%d-%m-%Y")
    filename = todayDate+".txt"
    newFile = open(filename,"a")
    newFile.close()
    return filename

#Clear Duplicate twitter handles from the file and return the filename
def clearDuplicatesFromFile():
    newFile = createFile()
    cmd = 'cat users.txt | sort | uniq >' + newFile
    subprocess.Popen(cmd,shell=True)
    return newFile
