#!/bin/bash/python
import time
import tweepy
import UserFile
import Authenticate
import ConfigParser
from tweepy import OAuthHandler

#Send out tweets to users
def sendTweets(api,tweet,filename):
    #[send out tweets]
    users = UserFile.getUserFromFile(filename)
    for user in users:
        usertweet= "@"+user+" "+tweet
        time.sleep(10)
        status = api.update_status(status=usertweet)


#Read tweet text from file
def readTweet():
    tweetFile = open("tweet.txt","r")
    tweetFileContent = tweetFile.read()
    tweetFile.close()
    return tweetFileContent

def main():
    cnf = Authenticate.getConfig()
    api = Authenticate.getAPI(cnf)
    tweet = readTweet()
    userfilename = UserFile.clearDuplicatesFromFile()
    #userfilename = 'test-users.txt'
    sendTweets(api,tweet,userfilename)

if __name__ == "__main__":
    main()
