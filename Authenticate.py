import ConfigParser
import tweepy
from tweepy import OAuthHandler

# Get configuration object
def getConfig():
    config = ConfigParser.ConfigParser()
    config.read("config.cnf")
    return config

#Get authenticated API object
def getAPI(config):
    auth = OAuthHandler(config.get('twitterKeys','consumer_key'),config.get('twitterKeys','consumer_secret'))
    auth.set_access_token(config.get('twitterKeys','access_token'),config.get('twitterKeys','access_secret'))
    return tweepy.API(auth)
