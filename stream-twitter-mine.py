#!/bin/bash/python
import json
import ssl
import time
import tweepy
import UserFile
import Authenticate
import ConfigParser
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
from requests.exceptions import Timeout, ConnectionError
from requests.packages.urllib3.exceptions import ReadTimeoutError

# This is the listener, resposible for receiving data
class tweetsListener(StreamListener):
    def on_status(self, status):
        try:
            if (status.place.country_code=='IN'):
                if ('#bigdata' or '#hadoop' or '#cloudera' or '#hive' or '#impala') in status.text:
                    print status.text
                    print status.place.country_code
                    UserFile.writeUserToFile(status.user.screen_name)

        except(Timeout, ssl.SSLError, ReadTimeoutError, ConnectionError) as exc:
            print "Timeout or Connection error occured!"

    def on_error(self,status):
        print status
        return False

    def stopListening(self):
        self.continueListening = False


if __name__=="__main__":

    config = Authenticate.getConfig()

    auth = OAuthHandler(config.get('twitterKeys','consumer_key'),config.get('twitterKeys','consumer_secret'))
    auth.set_access_token(config.get('twitterKeys','access_token'),config.get('twitterKeys','access_secret'))

    myTweetListener = tweetsListener()
    myStream = tweepy.Stream(auth,listener=tweetsListener())

    print "Fetching tweets"
    while True:
        try:
            #bottom-left-longitude, bottom-left-latitude, top-right-longitude, top-right-latitude.
            #NCR geobox=[76.03,28.58,78.00,28.66]
            #SFO geobox for testing purposes=[-122.75,36.8,-121.75,37.8]
            #The stream captures tweets from Indian Subcontinent
            myStream.filter(locations=[70.00,8.00,90.00,30.00])

        except (Timeout, ssl.SSLError, ReadTimeoutError, ConnectionError) as exc:
            print exc
            print "Timeout or Connection error occured! \n Sleeping for 1 min"
            time.sleep(60)

        except Exception,e:
            print e
            print "2 mins timeout: " + time.ctime()
            time.sleep(2*60)

        except KeyboardInterrupt:
            myTweetListener.stopListening()
            print "\nListening stopped due to Keyboard Interruption"
